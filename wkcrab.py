# -*- coding: utf-8 -*-
#
#       Copyright 2013 Gerardo Marset <gammer1994@gmail.com>
#
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 3 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.

from os import path
import json
from urllib import urlopen, urlretrieve
from aqt import qt, mw, utils
from anki.importing import noteimp

# Stuff that you probably want to change.
API_KEY = "Your API Key"
DECK_NAME = "WaniKani"

# Stuff that you probably shouldn't change.
RADICAL_MODEL_NAME = "WaniKani Radical Color"
KANJI_MODEL_NAME = "WaniKani Kanji Color"
VOCABULARY_MODEL_NAME = "WaniKani Vocabulary Color"
API_URL = "https://www.wanikani.com/api/v1.1/user"
BURNED_REVIEW_TIME = 14515200  # ~5.5 months.
CSS = (".card { font-family: arial; font-size: 20px; text-align: center; color: black; background-color: white; }\n"
       "p { margin: 0;}\n"
       "p.radical { font-size:100px; background-color: #0af; color: white; }\n"
       "p.kanji { font-size:100px; background-color: #f0a; color: white; }\n"
       "p.vocab { font-size:100px; background-color: #a0f; color: white; }\n"
       "p.reading { padding: 0.4em; color: white; background-color: #2d2d2d; }\n"
       "p.meaning { padding: 0.4em; color: black; background-color: #e8e8e8; }\n"
       "p.answer { font-size:30px; }")


class WKCrabDialog(qt.QDialog):
    def __init__(self, parent=mw):
        qt.QDialog.__init__(self, parent, qt.Qt.Window)
        self.setWindowTitle("Import WaniKani Burned Items")

        label_a = qt.QLabel("API key: ")
        label_b = qt.QLabel("Import to deck: ")
        buttons = qt.QDialogButtonBox(qt.QDialogButtonBox.Ok |
                                      qt.QDialogButtonBox.Cancel)

        self.api_key = qt.QLineEdit()
        self.api_key.setText(API_KEY)
        self.deck_name = qt.QLineEdit()
        self.deck_name.setText(DECK_NAME)

        v = qt.QVBoxLayout()
        v.addWidget(label_a)
        v.addWidget(self.api_key)
        v.addWidget(label_b)
        v.addWidget(self.deck_name)
        v.addWidget(buttons)
        self.setLayout(v)

        self.connect(buttons.button(qt.QDialogButtonBox.Ok),
                     qt.SIGNAL("clicked()"), self.accept)
        self.connect(buttons.button(qt.QDialogButtonBox.Cancel),
                     qt.SIGNAL("clicked()"), self.reject)

        self.exec_()

    def accept(self):
        if mw.col.decks.byName(self.deck_name.text()) is None and \
           not utils.askUser("The given deck doesn't exist. Create it?", self,
                             defaultno=True):
            return qt.QDialog.accept(self)
        deck_id = mw.col.decks.id(self.deck_name.text())

        name = api_get(self.api_key.text(),
                       "user-information")["user_information"]["username"]
        if utils.askUser("Importing burned items for the user \"{}\" "
                         "into the deck \"{}\"\n"
                         "Is this correct?".
                         format(name, self.deck_name.text()), self,
                         defaultno=True):
            wkcrab(self.api_key.text(), deck_id)

        mw.deckBrowser.show()
        return qt.QDialog.accept(self)


class WKImporter(noteimp.NoteImporter):
    def __init__(self, col, key):
        noteimp.NoteImporter.__init__(self, col, key)
        self.allowHTML = True

    def foreignNotes(self):
        notes = []
        for note_data in self.data:
            if note_data["stats"] is None or not note_data["stats"]["burned"]:
                continue
            notes.append(self.make_note(note_data))
        return notes

    def make_note(self, note_data):
        note = noteimp.ForeignNote()
        note.tags.append("wk_level{}".format(str(note_data["level"]).
                                             zfill(2)))
        return note


class WKRadicalImporter(WKImporter):
    def __init__(self, col, key):
        WKImporter.__init__(self, col, key)
        self.data = api_get(self.file, "radicals")["requested_information"]
        self.images = {}

    def fields(self):
        return 2

    def make_note(self, note_data):
        note = WKImporter.make_note(self, note_data)
        if note_data["character"] is None:
            note.fields.append("<img src=\"wk_{}.png\">".
                               format(note_data["meaning"]))
            self.images["wk_{}.png".format(note_data["meaning"])] = \
                    note_data["image"]
        else:
            note.fields.append(note_data["character"])
        note.fields.append(note_data["meaning"].replace("-", " "))
        note.tags.append("wk_radical")
        return note


class WKKanjiImporter(WKImporter):
    def __init__(self, col, key):
        WKImporter.__init__(self, col, key)
        self.data = api_get(self.file, "kanji")["requested_information"]

    def fields(self):
        return 3

    def make_note(self, note_data):
        note = WKImporter.make_note(self, note_data)
        note.fields.append(note_data["character"])
        note.fields.append(note_data["meaning"])
        note.fields.append(note_data[note_data["important_reading"]])
        note.tags.append("wk_kanji")
        return note


class WKVocabularyImporter(WKImporter):
    def __init__(self, col, key):
        WKImporter.__init__(self, col, key)
        self.data = api_get(self.file,
                            "vocabulary")["requested_information"]["general"]

    def fields(self):
        return 3

    def make_note(self, note_data):
        note = WKImporter.make_note(self, note_data)
        note.fields.append(note_data["character"])
        note.fields.append(note_data["meaning"])
        note.fields.append(note_data["kana"])
        note.tags.append("wk_vocabulary")
        return note


def radical_img_download(images):
    d = path.join(mw.pm.profileFolder(), "collection.media")
    for fname, url in images.iteritems():
        urlretrieve(url, path.join(d, fname))


def api_get(*args):
    f = urlopen("/".join((API_URL,) + args))
    j = f.read()
    f.close()
    try:
        j = json.loads(j)
    except ValueError:
        raise ValueError(j)
    else:
        try:
            error = j["error"]
        except KeyError:
            return j
        else:
            raise ValueError(error["message"])


def wkcrab(api_key, deck_id):
    model_manager = mw.col.models
    mw.col.decks.select(deck_id)

    # Radicals.
    model = model_manager.byName(RADICAL_MODEL_NAME)
    if not model:
        model = model_manager.new(RADICAL_MODEL_NAME)
        model_manager.addField(model, model_manager.newField("Radical"))
        model_manager.addField(model, model_manager.newField("Name"))
        template = model_manager.newTemplate("WaniKani Radical Name")
        template["qfmt"] = "<p class=\"radical\">{{Radical}}</p>\n"\
                "<p class=\"meaning\">Radical <b>name</b>?</p>"
        template["afmt"] = "<p class=\"answer\">{{Name}}</p>"
        model_manager.addTemplate(model, template)
        model_manager.add(model)
    model_manager.save(model)
    model_manager.setCurrent(model)
    model["did"] = deck_id
    model["css"] = CSS
    i = WKRadicalImporter(mw.col, api_key)
    i.initMapping()
    i.run()
    radical_img_download(i.images)

    # Kanji.
    model = model_manager.byName(KANJI_MODEL_NAME)
    if not model:
        model = model_manager.new(KANJI_MODEL_NAME)
        model_manager.addField(model, model_manager.newField("Kanji"))
        model_manager.addField(model, model_manager.newField("Meaning"))
        model_manager.addField(model, model_manager.newField("Reading"))
        template = model_manager.newTemplate("WaniKani Kanji Meaning")
        template["qfmt"] = "<p class=\"kanji\">{{Kanji}}</p>\n"\
                "<p class=\"meaning\">Kanji <b>meaning</b>?</p>"
        template["afmt"] = "<p class=\"answer\">{{Meaning}}</p>"
        model_manager.addTemplate(model, template)
        template = model_manager.newTemplate("WaniKani Kanji Reading")
        template["qfmt"] = "<p class=\"kanji\">{{Kanji}}</p>\n"\
                "<p class=\"reading\">Kanji <b>reading</b>?"
        template["afmt"] = "<p class=\"answer\">{{Reading}}</p>"
        model_manager.addTemplate(model, template)
        model_manager.add(model)
    model_manager.save(model)
    model_manager.setCurrent(model)
    model["did"] = deck_id
    model["css"] = CSS
    i = WKKanjiImporter(mw.col, api_key)
    i.initMapping()
    i.run()

    # Vocabulary.
    model = model_manager.byName(VOCABULARY_MODEL_NAME)
    if not model:
        model = model_manager.new(VOCABULARY_MODEL_NAME)
        model_manager.addField(model, model_manager.newField("Vocabulary"))
        model_manager.addField(model, model_manager.newField("Meaning"))
        model_manager.addField(model, model_manager.newField("Reading"))
        template = model_manager.newTemplate("WaniKani Vocabulary Meaning")
        template["qfmt"] = "<p class=\"vocab\">{{Vocabulary}}</p>\n"\
                "<p class=\"meaning\">Vocabulary <b>meaning</b>?</p>"
        template["afmt"] = "<p class=\"answer\">{{Meaning}}</p>"
        model_manager.addTemplate(model, template)
        template = model_manager.newTemplate("WaniKani Vocabulary Reading")
        template["qfmt"] = "<p class=\"vocab\">{{Vocabulary}}</p>\n"\
                "<p class=\"reading\">Vocabulary <b>reading</b>?</p>"
        template["afmt"] = "<p class=\"answer\">{{Reading}}</p>"
        model_manager.addTemplate(model, template)
        model_manager.add(model)
    model_manager.save(model)
    model_manager.setCurrent(model)
    model["did"] = deck_id
    model["css"] = CSS
    i = WKVocabularyImporter(mw.col, api_key)
    i.initMapping()
    i.run()

# Add the option to the tools menu.
action = qt.QAction("Import WaniKani Burned Items...", mw)
mw.connect(action, qt.SIGNAL("triggered()"), WKCrabDialog)
mw.form.menuTools.addAction(action)
